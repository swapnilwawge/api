<?php

namespace Application\Controllers\Tags;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use Eos\CTags;

class TagsController {

	public function getTags( Request $request, Response $response ) {
		try {
			$arrmixReturnReponse                 = [];
			$arrmixReturnReponse['responseCode'] = '200'; // We can define the constants for success codes
			$arrmixReturnReponse['message']      = '';

			$objTag = new CTags();
			$arrobjResponse = $objTag->getTagList();

			$arrmixReturnReponse['result'] = [];
			if( 0 == $arrobjResponse->num_rows ) {
				$arrmixReturnReponse['message'] = 'No Tags Available';
				return json_encode( $arrmixReturnReponse );
			}

			$i = 0;
			while( $row = $arrobjResponse->fetch_assoc() ) {
				$arrmixReturnReponse['result'][$i]['id'] = $row['id'];
				$arrmixReturnReponse['result'][$i]['title'] = $row['title'];
				$i++;
			}

		} catch( Exception $e) {
			// LOG THE ERROR IN NEW RELIC FOR INSTRUMENTATION
			$arrmixReturnReponse['responseCode'] = '100'; // We can define the constants for success codes
			$arrmixReturnReponse['message']     = 'Something Went Wrong! Please try again';
		}

		return json_encode( $arrmixReturnReponse );
	}

	public function addTag( Request $request, Response $response ) {
		try {
			$arrmixReturnReponse['responseCode'] = '200'; // We can define the constants for success codes
			$arrmixReturnReponse['message']     = 'Tag inserted successfully';

			$arrmixArgs = $request->getParsedBody();

			$objTag = new CTags();
			$arrmixArgs = $objTag->addTag( $arrmixArgs );

			if( 0 == count( $arrmixArgs ) ) {
				$arrmixReturnReponse['responseCode'] = '100'; // We can define the constants for success codes
				$arrmixReturnReponse['message']      = 'Something Went Wrong! Please try again';
				return json_encode( $arrmixReturnReponse );
			}

			$arrmixReturnReponse['result'] = [];
			$arrmixReturnReponse['result']['id'] = $arrmixArgs['id'];
			$arrmixReturnReponse['result']['title'] = $arrmixArgs['title'];

		} catch( Exception $e) {
			// LOG THE ERROR IN NEW RELIC FOR INSTRMENTATION
			$arrmixReturnReponse['responseCode'] = '100'; // We can define the constants for success codes
			$arrmixReturnReponse['message']     = 'Something Went Wrong! Please try again';
		}

		return json_encode( $arrmixReturnReponse );
	}

	public function deleteTag( Request $request, Response $response ) {
		try {
			$arrmixReturnReponse['responseCode'] = '200'; // We can define the constants for success codes
			$arrmixReturnReponse['message']     = 'Tag deleted successfully';

			$arrmixArgs = $request->getParsedBody();

			$objTags = new CTags();
			$boolIsSuccess = $objTags->deleteTag( $arrmixArgs );

			if( true == $boolIsSuccess ) {
				return json_encode( $arrmixReturnReponse );
			}

		} catch( Exception $e) {
			// LOG THE ERROR IN NEW RELIC FOR INSTRUMENTATION
			$arrmixReturnReponse['responseCode'] = '100'; // We can define the constants for success codes
			$arrmixReturnReponse['message']     = 'Something Went Wrong! Please try again';
		}

		return json_encode( $arrmixReturnReponse );
	}

}