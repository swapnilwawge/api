<?php

namespace Application\Controllers\Notes;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use Eos\CNotes;

class NotesController {

	public function getNotes( Request $request, Response $response ) {
		try {
			$arrmixReturnReponse                 = [];
			$arrmixReturnReponse['responseCode'] = '200'; // We can define the constants for success codes
			$arrmixReturnReponse['message']      = '';

			$objNotes = new CNotes();
			$arrobjResponse = $objNotes->getNoteList();

			$arrmixReturnReponse['result'] = [];
			if( 0 == $arrobjResponse->num_rows ) {
				$arrmixReturnReponse['message'] = 'No Notes Available';
				return json_encode( $arrmixReturnReponse );
			}

			$i = 0;
			while( $row = $arrobjResponse->fetch_assoc() ) {
				$arrmixReturnReponse['result'][$i]['id'] = $row['id'];
				$arrmixReturnReponse['result'][$i]['title'] = $row['title'];
				$arrmixReturnReponse['result'][$i]['description'] = $row['description'];
				$arrmixReturnReponse['result'][$i]['dateTime'] = $row['created_on'];
				$arrmixReturnReponse['result'][$i]['tags'] = [ $row['tag_title']];
				$i++;
			}

		} catch( Exception $e) {
			// LOG THE ERROR IN NEW RELIC FOR INSTRUMENTATION
			$arrmixReturnReponse['responseCode'] = '100'; // We can define the constants for success codes
			$arrmixReturnReponse['message']     = 'Something Went Wrong! Please try again';
		}

		return json_encode( $arrmixReturnReponse );
	}

	public function addNote( Request $request, Response $response ) {
		try {
			$arrmixReturnReponse['responseCode'] = '200'; // We can define the constants for success codes
			$arrmixReturnReponse['message']     = 'Note inserted successfully';

			$arrmixArgs = $request->getParsedBody();

			$objNotes = new CNotes();
			$arrmixArgs = $objNotes->addNote( $arrmixArgs );

			if( 0 == count( $arrmixArgs ) ) {
				$arrmixReturnReponse['responseCode'] = '100'; // We can define the constants for success codes
				$arrmixReturnReponse['message']      = 'Something Went Wrong! Please try again';
				return json_encode( $arrmixReturnReponse );
			}

			$arrmixReturnReponse['result'] = [];
			$arrmixReturnReponse['result']['id'] = $arrmixArgs['id'];
			$arrmixReturnReponse['result']['title'] = $arrmixArgs['title'];
			$arrmixReturnReponse['result']['description'] = $arrmixArgs['description'];
			$arrmixReturnReponse['result']['dateTime'] = $arrmixArgs['created_on'];
			$arrmixReturnReponse['result']['tags'] = $arrmixArgs['tag_title'];

		} catch( Exception $e) {
			// LOG THE ERROR IN NEW RELIC FOR INSTRMENTATION
			$arrmixReturnReponse['responseCode'] = '100'; // We can define the constants for success codes
			$arrmixReturnReponse['message']     = 'Something Went Wrong! Please try again';
		}

		return json_encode( $arrmixReturnReponse );
	}

}