<?php

namespace Eos;

use mysql_xdevapi\Exception;
use Routes\Defines;

class CNotes extends Defines {
	protected $m_objConn;

	public function __construct() {
		parent::__construct();

		/*
		 * For coding test purpose used the legacy database/connection
		 * We should use the PDO or nette which helps to write query easily
		 * */

		try {
			$this->m_objConn = mysqli_connect( $this->m_strDBServerName, $this->m_strDBUsername, $this->m_strDBUPassword, $this->m_strDB );
		} catch( Exception $e) {
			echo 'Connection failed: ' . $e->getMessage();
		}
	}

	public function getNoteList() {
		$strSql = 'SELECT
						n.id, n.title, n.description, n.created_on,
						 group_concat(t.title) as tag_title
					FROM
						notes n
						LEFT JOIN ( note_tags nt JOIN tags t ON t.id = nt.tag_id AND t.is_active = 1 ) ON nt.note_id = n.id 
					GROUP by
						n.id, n.title, n.description, n.created_on
					ORDER BY
						n.id DESC';

		$arrobjResponse = $this->m_objConn->query( $strSql );

		$this->m_objConn->close(); // Close database connection here
		return $arrobjResponse;
	}

	public function addNote( $arrmixArgs ) {
		/*
		 * Mysql db does not support database transaction, but if we use other db's ex: postgres etc
		 *  we can insert data in notes and tags table in a transaction, so that if anyone of the insert statement fails, we can rollback the transaction
		 * */

		// escape strings to prevent any database scrutiny
		$strDateTime = date(' Y-m-d H:i:s');

		$strSql = "INSERT INTO notes( title, description, created_on )
					VALUES( '" . $arrmixArgs['title'] . "', '" . $arrmixArgs['description'] . "', '" . $strDateTime . "' )";

		if( $this->m_objConn->query( $strSql ) === true ) {
			$intNoteId = $this->m_objConn->insert_id;

			foreach( $arrmixArgs['tags'] as $intTagId ) {
				$strNoteTagSql = "INSERT INTO note_tags( note_id, tag_id )
					VALUES( " . $intNoteId . ", " . $intTagId . " )";

				if( $this->m_objConn->query( $strNoteTagSql ) === false ) {
					break;
				}
			}

			$strSelectTagSql = 'SELECT
							title
						FROM
							tags 
						WHERE
							id IN ( ' . implode( ',', $arrmixArgs['tags'] ) . ' ) ';

			$arrobjTagsDetails = $this->m_objConn->query( $strSelectTagSql );

			$arrmixArgs['tag_title'] = [];
			if( 0 < $arrobjTagsDetails->num_rows ) {
				while( $row = $arrobjTagsDetails->fetch_assoc() ) {
					$arrmixArgs['tag_title'][] = $row['title'];
				}
			}

			$this->m_objConn->close(); // Close database connection here

			$arrmixArgs['id']         = $intNoteId;
			$arrmixArgs['created_on'] = $strDateTime;

			return $arrmixArgs;


		} else {
			return $this->m_objConn->error;
		}
	}
}
