<?php

namespace Eos;

use mysql_xdevapi\Exception;
use Routes\Defines;

class CTags extends Defines {
	protected $m_objConn;

	public function __construct() {
		parent::__construct();

		/*
		 * For coding test purpose used the legacy database/connection
		 * We should use the PDO or nette which helps to write query easily
		 * */

		try {
			$this->m_objConn = mysqli_connect( $this->m_strDBServerName, $this->m_strDBUsername, $this->m_strDBUPassword, $this->m_strDB );
		} catch( Exception $e) {
			echo 'Connection failed: ' . $e->getMessage();
		}
	}

	public function getTagList() {
		$strSql = 'SELECT
						t.id, t.title, t.created_on
					FROM
						tags t
					WHERE
						is_active = 1
					ORDER BY
						t.id DESC';

		$arrobjResponse = $this->m_objConn->query( $strSql );

		$this->m_objConn->close(); // Close database connection here
		return $arrobjResponse;
	}

	public function addTag( $arrmixArgs ) {
		// escape strings to prevent any database scrutiny

		$strDateTime = date(' Y-m-d H:i:s');

		$strSql = "INSERT INTO tags( title, created_on )
					VALUES( '" . $arrmixArgs['title'] . "', '" . $strDateTime . "' )";

		if( $this->m_objConn->query( $strSql ) === true ) {
			$intTagId = $this->m_objConn->insert_id;

			$arrmixArgs['id']         = $intTagId;
			$arrmixArgs['created_on'] = $strDateTime;

			$this->m_objConn->close(); // Close database connection here
			return $arrmixArgs;
		} else {
			return $this->m_objConn->error;
		}
	}

	public function deleteTag( $arrmixArgs ) {
		$strUpdateSql = 'UPDATE
								tags
							SET
								is_active = 0
							WHERE
							id = ' . ( int ) $arrmixArgs['id'];

		// we can either check if the above sql runs successfully or the number of records affected
		if( $this->m_objConn->query( $strUpdateSql ) === true ) {
			$strResult = true;
		} else {
			$strResult = $this->m_objConn->error;
		}

		$this->m_objConn->close(); // Close database connection here
		return $strResult;
	}
}
