<?php

use Application\Controllers\{Notes, Tags};

$app->group( '/notes', function () {
		$this->get( '/getNotes', Notes\NotesController::class . ':getNotes' );
		$this->post( '/addNote', Notes\NotesController::class . ':addNote' );
		// We can add the validation to the request parameter for each API call
	});

$app->group( '/tags', function () {
	$this->get( '/getTags', Tags\TagsController::class . ':getTags' );
	$this->post( '/addTag', Tags\TagsController::class . ':addTag' );
	$this->post( '/deleteTag', Tags\TagsController::class . ':deleteTag' );
	// We can add the validation to the request parameter for each API call
});