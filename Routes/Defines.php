<?php

/*
 * Store database credentials in this file
 *
 * */

namespace Routes;

class Defines {

	protected $m_strDBServerName;
	protected $m_strDBUsername;
	protected $m_strDBUPassword;
	protected $m_strDB;

	public function __construct() {
		$this->m_strDBServerName = 'localhost';
		$this->m_strDBUsername = 'root';
		$this->m_strDBUPassword = '';
		$this->m_strDB = 'intTest';

	}
}