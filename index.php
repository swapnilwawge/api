<?php

header( 'Access-Control-Allow-Origin:*' );
header( 'Access-Control-Allow-Headers:*' );
header( 'Access-Control-Allow-Methods:*' );

$strPathRoot = __DIR__ ;
require $strPathRoot . '/vendor/autoload.php';

$app = new Slim\App();

require $strPathRoot . '/Routes/Routes.php';
/*
 * We can load the database connection here by storing the details in a config or .env file and then autoloading it
 * New relic / monolog dependencies can be added here
 * */

$app->run();